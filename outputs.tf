# outputs.tf defines outputs for the module

output "secret" {
  description = "Secret resource"
  value       = google_secret_manager_secret.secret

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}

output "project" {
  description = "Project containing secret"
  value       = local.project

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}

output "region" {
  description = "Region containing secret"
  value       = local.region

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}

output "secret_id" {
  description = "Id of secret"
  value       = var.secret_id

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}

output "version" {
  description = "Current version of secret"
  value       = local.secret_version

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}

output "url" {
  description = "A Berglas-style sm://... URL for the current version of the secret."
  value       = local.secret_url

  # Ensure that outputs also depend on the appropriate IAM policies being set.
  depends_on = [
    google_secret_manager_secret_iam_member.secret_accessors,
    google_secret_manager_secret_iam_member.iam_policy_member,
  ]
}
