# Google Cloud Secrets Manager

This module will create a Google Secret Manager secret and associated secret version with a passed
content.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

The id, the version and the url for the secret created are available from the project
[outputs](outputs.tf). Configuration variables are documented in [variables.tf](variables.tf).

## Examples

### A basic secret and secret version

The following example creates a secret with the ID `test-secret` and a corresponding secret version
containing the data `super-secret-data`.

```tf
module "secret" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "{REPLACE WITH A VERSION SPECIFIER}"  # E.g. "<5.0.0"

  project   = "some-project-id"
  region    = "europe-west2"
  secret_id = "test-secret"
}
```

The `project` and/or `region` variables may be omitted. If so, the project and region associated
with the `google` provider will be used.

### A basic secret without a secret version

The following example creates a secret with the ID `test-secret` but a corresponding secret version
is not created as the `manage_secret_versions` variable is set to `false`. This is useful in
situations where you want to manage the secret object in Terraform but do not want Terraform to
manage the secret data.

```tf
module "secret" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "{REPLACE WITH A VERSION SPECIFIER}"  # E.g. "<5.0.0"

  project                = "some-project-id"
  region                 = "europe-west2"
  secret_id              = "test-secret"
  manage_secret_versions = false
}
```

In earlier versions of this module it was sufficient to just exclude `secret_data`. The new
solution reduces the likelihood of getting the Terraform error:

```
The "count" value depends on resource attributes that cannot be determined until apply, so Terraform cannot predict how many instances will be created.
```

### Configure secret accessor IAM bindings

The following example grants the `my_secret_accessor` service account the
`roles/secretmanager.secretAccessor` role on the newly create secret object.

```tf
resource "google_service_account" "secret_accessor" {
    account_id  = "my_secret_accessor"
    description = "My secret accessor service account"
    project     = local.project
}

module "secret" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "{REPLACE WITH A VERSION SPECIFIER}"  # E.g. "<5.0.0"

  project          = "some-project-id"
  region           = "europe-west2"
  secret_id        = "test-secret"
  secret_accessors = {
    arbitrary_key_name = "serviceAccount:${google_service_account.secret_accessor.email}"
  }
}
```

### Configuring other IAM permissions

You may want to configure additional IAM permissions beyond accessing secrets. For example, you may
want to allow a service account to *add* additional versions of a secret. This can be done via the
`iam_policy` variable:

```tf
resource "google_service_account" "secret_adder" {
    account_id  = "my_secret_adder"
    description = "My secret adder service account"
    project     = "some-project-id"
}

module "secret" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "{REPLACE WITH A VERSION SPECIFIER}"  # E.g. "<5.0.0"

  project   = google_service_account.secret_adder.project
  region    = "europe-west2"
  secret_id = "test-secret"

  iam_policy = {
    "roles/secretmanager.secretVersionAdder": [
       "serviceAccount:${google_service_account.secret_adder.email}"
    ]
  }
}
```

Note that, for future-proofing, you can add the `roles/secretmanager.secretAccessor` permissions via
`iam_policy` instead of `secret_accessors` if you prefer. The `secret_accessors` variable may be
removed in a later version of the module.

### Further examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/root-example](examples/root-example/).
