# locals.tf defines common expressions used by the module.

locals {
  # If anyone can think of a less horrible way of doing this, let me know!
  secret_version = var.secret_data != null ? "#${split("/", google_secret_manager_secret_version.secret[0].name)[5]}" : ""

  # A Berglas-style sm://... URL for the current version of the secret.
  secret_url = "sm://${local.project}/${var.secret_id}${local.secret_version}"

  # A flattened map of IAM role bindings. Expands into a map from a stable id to a map containing {
  # role, member }. The stable id is opaque but is a) consistent for a given role and member binding
  # and b) human-readable.
  iam_bindings = { for binding in flatten([
    for role, members in var.iam_policy : [
      for member in members : {
        role   = role
        member = member
      }
    ]
    ]) :
    "${binding.role}--${binding.member}" => binding
  }

  # Project and region to create secrets in. If not provided via variables, use the project and
  # version for the Google client.
  project = coalesce(var.project, data.google_client_config.current.project)
  region  = coalesce(var.region, data.google_client_config.current.region)
}
