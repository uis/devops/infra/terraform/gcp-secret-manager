# Simple example

This is a simple example of storing a cluster named "test_secret" into a GCP
project. Specify the project to deploy into on the command line. So, for
example, to deploy to the project ``my-project``:

```console
$ terraform init
$ terraform apply -var project=my-project
```

In this example, terraform is configured to use default application credentials.
For Google APIs and these credentials should correspond to a user with owner or
editor access to the target project. You can use the ``gcloud`` command line
tool to set your personal credentials as application default credentials. See
the ``gcloud auth application-default`` command output for more information.
