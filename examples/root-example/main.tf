# A Secret Manager secret which holds the secret "test_scret"
module "secret" {
  source      = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version     = "<5.0.0"
  project     = var.project
  region      = "europe-west2"
  secret_id   = "test_secret"
  secret_data = "test_secret"
}
