# variables.tf defines inputs for the module.

variable "project" {
  description = "Project containing the secret."
  type        = string
  default     = null
}

variable "region" {
  description = "Restrict secret to this region"
  type        = string
  default     = null
}

variable "secret_id" {
  description = "Secret name"
  type        = string
}

variable "secret_data" {
  description = "Optional. Payload for secret."
  type        = string
  default     = null
}

variable "secret_accessors" {
  description = <<EOI
Optional. A map of members to grant the secretmanager.secretAccessor role to.

This must be in the format {<arbitrary_key_name> = <member email>}
EOI
  type        = map(string)
  default     = {}
}

variable "manage_secret_versions" {
  description = <<EOI
Optional. When false, the secret will be created with no versions.
EOI
  type        = bool
  default     = true
}

variable "iam_policy" {
  description = <<EOI
    IAM policy bindings for the created secret. A map from role name to a list of members.
  EOI
  type        = map(list(string))
  default     = {}
}
