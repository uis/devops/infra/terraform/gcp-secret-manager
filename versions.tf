# versions.tf defines minimum provider versions for the module

terraform {
  required_providers {
    google      = ">= 3.63"
    google-beta = ">= 3.63"
  }

  required_version = ">= 0.13"
}
