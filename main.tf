# main.tf defines the main resources of the module.

data "google_client_config" "current" {}

resource "google_secret_manager_secret" "secret" {
  project   = local.project
  secret_id = var.secret_id

  replication {
    user_managed {
      replicas {
        location = local.region
      }
    }
  }
}

resource "google_secret_manager_secret_version" "secret" {
  count = var.manage_secret_versions ? 1 : 0

  secret      = google_secret_manager_secret.secret.id
  secret_data = var.secret_data
}

moved {
  from = google_secret_manager_secret_version.secret
  to   = google_secret_manager_secret_version.secret[0]
}

resource "google_secret_manager_secret_iam_member" "secret_accessors" {
  for_each = var.secret_accessors

  role      = "roles/secretmanager.secretAccessor"
  secret_id = google_secret_manager_secret.secret.id
  member    = each.value
}

# IAM bindings for the secret from var.iam_policy.
resource "google_secret_manager_secret_iam_member" "iam_policy_member" {
  for_each = local.iam_bindings

  secret_id = google_secret_manager_secret.secret.id
  role      = each.value.role
  member    = each.value.member
}
