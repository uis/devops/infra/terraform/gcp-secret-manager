# Changelog

## [4.5.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.5.0...4.5.1) (2024-08-27)


### Bug Fixes

* remove length validation for 'secret_id' variable ([3ee5582](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/3ee55824077dbdf0b85cd5eeb3303d5a597f1935))

## [4.5.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.4.3...4.5.0) (2024-08-27)


### Features

* add length validation for 'secret_id' variable ([4844aef](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/4844aef7f75c42f898265da745c518d94bccec54))

## [4.4.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.4.2...4.4.3) (2024-08-20)

## [4.4.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.4.1...4.4.2) (2024-08-20)

## [4.4.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.4.0...4.4.1) (2024-07-24)

## [4.4.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.3.2...4.4.0) (2024-05-02)


### Features

* only constrain the lower version and leave the maximum version open-ended ([f22b18f](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/f22b18fb702813c853c74dae58d09316a51af399))

## [4.3.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.3.1...4.3.2) (2024-01-31)


### Bug Fixes

* **gitlab-ci:** re-enable check_latest_tag_in_changelog job ([a510ca9](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/a510ca9d41e42c87bb5a97763238c2221605811e))

## [4.3.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.3.0...4.3.1) (2024-01-08)


### Bug Fixes

* **gitlab-ci:** disable check_latest_tag_in_changelog job ([2295292](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/2295292bf0232138eaa6b2530a111d8f072c0e78)), closes [heads#L17](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/heads/issues/L17)

## [4.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/compare/4.2.0...4.3.0) (2024-01-08)


### Features

* add automated release management ([de8d367](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/de8d367ab48ecd1547526a3b74e51cfec75e6b7e))
* add commitlint CI testing jobs ([564f204](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/564f20468b83315589ae53a67216cd35f6e467a7))
* **CHANGELOG.md:** make CHANGELOG release-it friendly ([24e4997](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/24e499774bb0985aa1e3357c8ab5124c5c53e3f5))
* **README:** remove text on versioning ([da34fed](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager/commit/da34fed41679e80e98def5d98afed1f4ff9f4593))

## [4.2.0] - 2024-01-03

### Added

- The new `iam_policy` variable may now be used to bind IAM roles to members for the secret.
- The `project` and `region` variables may now be omitted to use the project and region associated
  with the Google client instead.

## [4.1.0] - 2024-01-03

### Fixed

- The README now shows use of the published terraform module rather than cloning from git directly.

## [4.0.0] - 2023-12-11

### Fixed

- Terraform errors when bootstrapping projects by adding a
  `manage_secret_versions` variable for enabling/disabling the managing of
  secret versions/payloads.

## [3.1.2] - 2023-11-02

### Added

- Added support to publish to GitLab Terraform registry when tagged using semver

## [3.1.1] - 2023-08-10

### Fixed

- Secret accessor IAM binding variable type.

## [3.1.0] - 2023-08-09

### Added

- Ability to create/manage secrets without also managing the secret version/payload.
- Ability to configure secret accessor IAM bindings.

## [3.0.0] - 2022-05-14
### Changed
 - Increased compatibility with new versions of the provider and Terraform.

## [2.0.0] - 2021-04-21
### Changed
 - Added minimum provider version which breaks compatibility with terraform 0.12.

## [1.0.0] - 2029-03-23
### Added
 - Initial version
